#!/usr/bin/env bash
# Creates the JSON structure, which can be feed into an scm-source.json file
# as described in
# http://stups.readthedocs.org/en/latest/user-guide/application-development.html

if ! hash git &>/dev/null; then
    echo 'Cannot generate scm-source.json as git is not installed'
    exit 1
fi

declare -r URL=$(git config --get remote.origin.url)
declare -r AUTHOR=$(git show --format='%aN (%aE)' --no-patch)
declare -r STATUS=$(git status -z)

REVISION=$(git show --format='%h' --no-patch)

if [[ -n "${STATUS}" ]]; then
    REVISION="${REVISION}-dirty"
fi

cat << EOF
{
    "url": "${URL}",
    "revision": "${REVISION}",
    "author": "${AUTHOR}",
    "status": "${STATUS}"
}
EOF
